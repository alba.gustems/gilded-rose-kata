import { expect } from 'chai';
import { Item, GildedRose } from '@/gilded-rose';

describe('Gilded Rose', () => {
  it('should foo', () => {
    const gildedRose = new GildedRose([new Item('foo', 0, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).to.equal('foo');
  });

  it('should decrease the quality of the product if it is not Sulfuras, Hand of Ragnaros', () => {
    const gildedRose = new GildedRose([new Item('foo', 0, 1)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(0);
  });

  it('should the quality of Sulfuras, Hand of Ragnaros always be 80', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 0, 80)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(80);
  });

  it('should not decrease the quality of the product if it is Sulfuras, Hand of Ragnaros', () => {
    const gildedRose = new GildedRose([new Item('Sulfuras, Hand of Ragnaros', 1, 1)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(1);
  });

  it('should the quality of an item never be more than 50', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 0, 50)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(50);
  });

  it('should the quality of an item never be more less than 0', () => {
    const gildedRose = new GildedRose([new Item('foo', 0, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(0);
  });

  it('should the quality decrease twice as fast if the sellIn is negative', () => {
    const gildedRose = new GildedRose([new Item('foo', -1, 2)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(0);
  });

  it('should increase the quality 1 if the name of the item is Backstage passes to a TAFKAL80ETC concert if sell in is above 10', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 11, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(1);
  });

  it('should increase the quality 2 if the name of the item is Backstage passes to a TAFKAL80ETC concert if sell in is between 5 and 10', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 10, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(2);
  });

  it('should increase the quality 3 if the name of the item is Backstage passes to a TAFKAL80ETC concert if sell in is between 0 and 5', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 5, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(3);
  });

  it('should set the quality to 0 if the name of the item is Backstage passes to a TAFKAL80ETC concert and sell negative', () => {
    const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', -10, 10)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(0);
  });

  it('should the quality of Aged Brie should always increase 1 as the sellIn is lower and above 0', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', 1, 2)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(3);
  });

  it('should the quality of Aged Brie should always increase 2 as the sellIn is lower or equal to 0', () => {
    const gildedRose = new GildedRose([new Item('Aged Brie', -10, 2)]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(4);
  });
});
