export class Item {
  name: string;
  sellIn: number;
  quality: number;

  constructor(name, sellIn, quality) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

export class GildedRose {
  items: Array<Item>;

  constructor(items = [] as Array<Item>) {
    this.items = items;
  }

  updateQuality() {
    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i].name === 'Sulfuras, Hand of Ragnaros') {
        continue
      }

      if (!(this.items[i].name != 'Aged Brie' && this.items[i].name != 'Backstage passes to a TAFKAL80ETC concert')) {
        if (this.items[i].quality < 50) {
          GildedRose.increaseQualityByAmount(this.items[i]);
          if (this.items[i].name == 'Backstage passes to a TAFKAL80ETC concert') {
            if (this.items[i].sellIn < 11) {
              GildedRose.increaseQualityByAmount(this.items[i]);
            }
            if (this.items[i].sellIn < 6) {
              GildedRose.increaseQualityByAmount(this.items[i]);
            }
          }
        }
      } else {
        GildedRose.decreaseQualityByAmount(this.items[i]);
      }

      GildedRose.decreaseSellIn(this.items[i]);

      if (this.items[i].sellIn >= 0) {
        continue;
      }
      if (this.items[i].name == 'Aged Brie') {
        GildedRose.increaseQualityByAmount(this.items[i]);
      } else {
        if (this.items[i].name == 'Backstage passes to a TAFKAL80ETC concert') {
          this.items[i].quality = this.items[i].quality - this.items[i].quality
        } else {
          GildedRose.decreaseQualityByAmount(this.items[i]);
        }
      }
    }

    return this.items;
  }

  private static decreaseQualityByAmount(item: Item) {
    if (item.quality > 0) {
      item.quality = item.quality - 1
    }
  }

  private static increaseQualityByAmount(item: Item) {
    if (item.quality < 50) {
      item.quality = item.quality + 1
    }
  }

  private static decreaseSellIn(item: Item) {
    item.sellIn = item.sellIn - 1;
  }
}
